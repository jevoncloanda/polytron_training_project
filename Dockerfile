FROM node:18-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install
COPY . .

# RUN apk add bash
# RUN cd ./db
# RUN npx knex migrate:latest
# RUN npx knex seed:run
# RUN cd ..

EXPOSE 3306
EXPOSE 3000

# ENTRYPOINT [ "npx", "knex migrate:latest" ]
CMD ["node", "index.js"]
# CMD ["/bin/bash", "/startup.sh"]