/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema.createTable('employees', (table) => {
        table.increments('employee_id').primary();
        
        table.integer('company_id').unsigned()
        .references('companies.company_id')
        .onDelete('CASCADE')
        .onUpdate('CASCADE');

        table.string('employee_name', 255);
        table.integer('employee_gender');
        table.date('employee_birthday');
        table.string('employee_picture', 100);
        table.string('employee_phone', 20);
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema.dropTable('employees')
};
