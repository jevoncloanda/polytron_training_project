/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function(knex) {
    return knex.schema.createTable('companies', (table) => {
        table.increments('company_id').primary();
        table.string('company_name', 50);
        table.string('company_phone', 20);
        table.string('company_address', 255);
    })
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function(knex) {
    return knex.schema.dropTable('companies')
};
