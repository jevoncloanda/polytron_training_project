/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('employees').del()
  await knex('employees').insert([
    {company_id: 1, employee_name: 'Jevon', employee_gender: 1, employee_birthday: '2003-06-12', employee_phone: '0812'},
    {company_id: 1, employee_name: 'Joshua', employee_gender: 1, employee_birthday: '2003-06-12', employee_phone: '0812'},
    {company_id: 2, employee_name: 'Nabil', employee_gender: 1, employee_birthday: '2003-06-12', employee_phone: '0812'},
    {company_id: 2, employee_name: 'Reza', employee_gender: 1, employee_birthday: '2003-06-12', employee_phone: '0812'},
    {company_id: 3, employee_name: 'Adit', employee_gender: 1, employee_birthday: '2003-06-12', employee_phone: '0812'},
    {company_id: 4, employee_name: 'Najla', employee_gender: 2, employee_birthday: '2003-06-12', employee_phone: '0812'}
  ]);
};
