/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('companies').del()
  await knex('companies').insert([
    {company_name: 'Polytron', company_phone: '1234', company_address: 'Wisma Barito 2'},
    {company_name: 'Tiktok', company_phone: '1111', company_address: 'Pacific Place'},
    {company_name: 'GoTo', company_phone: '0897', company_address: 'Jakarta'},
    {company_name: 'Harita', company_phone: '5566', company_address: 'Senayan'},
    {company_name: 'Djarum', company_phone: '9128', company_address: 'Bogor'},
  ]);
};
