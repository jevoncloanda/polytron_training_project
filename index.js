const express = require('express')
const expressLayouts = require('express-ejs-layouts')
const methodOverride = require('method-override')
const fs = require('fs')
const app = express()
const port = 3000;

const { body, validationResult, check } = require('express-validator')
// DB
const db = require('./db/connection')

// Moment for Date & Time
const moment = require('moment')

// Setup EJS
app.set('view engine', 'ejs')
app.use(expressLayouts)

// Accessing Static File (img, css, dll)
app.use(express.static('public'))
app.use(express.urlencoded({extended: true}))

// app.use(express.static(__dirname + '/public'))

// Setup Multer for File Manager
const path = require('path')
const multer = require('multer')
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/img/')
    },
    filename: (req, file, cb) => {
        cb(null, moment.now() + path.extname(file.originalname))
    }
})

const upload = multer({storage: storage})

// Method Override
app.use(methodOverride('_method'))


app.get('/', (req, res) => {
  res.redirect('/company')
})

// View Company Start
app.get('/company', async (req, res) => {
    const companies = await db.select().from('companies')
    res.render('company/view-companies', {title: 'Company', layout: 'layouts/main', companies})
})
// View Company End

// Create Company Start
app.get('/company/add', (req, res) => {
    const oldValue = {
        company_name: "",
        company_phone: "",
        company_address: ""
    }
    res.render('company/add-company', {title: 'Create Company', layout: 'layouts/main', oldValue}) 
})

app.post('/company', [
    check('company_name', 'Please fill Company Name').notEmpty(),
    check('company_phone', 'Please fill Company Phone').notEmpty(),
    check('company_address', 'Please fill Company Address').notEmpty(),
    check('company_phone', 'Company phone must be a number').isNumeric()
], (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        const oldValue = req.body
        // res.send(errors)
        res.render('company/add-company', {title: 'Create Company', layout: 'layouts/main', oldValue, errors: errors.array()})
    } else {
        db('companies').insert({
            company_name: req.body.company_name,
            company_phone: req.body.company_phone,
            company_address: req.body.company_address
        }).then(() => {
            // db.select().from('companies').then((companies) => {
            //     res.send(companies);
            // })
            res.redirect('/company')
        })
    }
})
// Create Company End

// Edit Company Start
app.get('/company/edit/:id', async (req, res) => {
    const company = await db('companies').where('company_id', req.params.id).first()
    // res.send(company)
    res.render('company/edit-company', {company, title: 'Edit Company', layout: 'layouts/main'})
})

app.put('/company', [
    check('company_name', 'Please fill Company Name').notEmpty(),
    check('company_phone', 'Please fill Company Phone').notEmpty(),
    check('company_address', 'Please fill Company Address').notEmpty()
], (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        const company = req.body
        res.render('company/edit-company', {title: 'Edit Company', layout: 'layouts/main', company, errors: errors.array()})
    } else {
        db('companies').where('company_id', req.body.id)
        .update({
            company_name: req.body.company_name,
            company_phone: req.body.company_phone,
            company_address: req.body.company_address
        }).then(() => {
            res.redirect('/company')
        })
    }
})
// Edit Company End

// Delete Company Start
app.delete('/company', (req, res) => {
    db('companies').where('company_id', req.body.id)
    .delete().then(() => {
        res.redirect('/company')
    })
})
// Delete Company End

// View Employee Start
app.get('/company/:id', async (req, res) => {
    const company = await db('companies').where('company_id', req.params.id).first()
    const employees = await db('employees')
    .join('companies', 'employees.company_id', 'companies.company_id')
    .where('companies.company_id', req.params.id)
    // res.send(company)
    res.render('employee/view-employees', {title: 'Employee List', layout: 'layouts/main', employees, company, moment})
})
// View Employee End

// Create Employee Start
app.get('/company/:id/employee/add', async (req, res) => {
    const company = await db('companies').where('company_id', req.params.id).first()
    const oldValue = {
        employee_name: "",
        employee_gender: null,
        employee_birthday: "",
        employee_picture: "",
        employee_phone: ""
    }
    res.render('employee/add-employee', {title: 'Create Employee', layout: 'layouts/main', oldValue, company}) 
})

app.post('/company/:id/employee', upload.single('employee_picture'), [
    check('employee_name', 'Please fill Employee Name').notEmpty(),
    check('employee_gender', 'Please fill Employee Gender').notEmpty(),
    check('employee_birthday', 'Please fill Employee Birthday').notEmpty(),
    check('employee_birthday', 'Birthday must be earlier than today').isBefore(),
    check('employee_phone', 'Please fill Employee Phone').notEmpty(),
    check('employee_phone', 'Phone must be a number').isNumeric()
], async (req, res) => {
    const company = await db('companies').where('company_id', req.params.id).first()
    const errors = validationResult(req)
    // res.send(req.file)
    let employeeFilename
    if (req.file) employeeFilename = req.file.filename
    if (!errors.isEmpty()) {
        const oldValue = req.body
        // res.send(errors)
        res.render('employee/add-employee', {title: 'Create Employee', layout: 'layouts/main', oldValue, errors: errors.array(), company})
    } else {
        db('employees').insert({
            company_id: company.company_id,
            employee_name: req.body.employee_name,
            employee_gender: req.body.employee_gender,
            employee_birthday: req.body.employee_birthday,
            employee_picture: employeeFilename,
            employee_phone: req.body.employee_phone
        }).then(() => {
            // db.select().from('employees').then((employees) => {
            //     res.send(employees);
            // })
            res.redirect('/company/' + company.company_id)
        })
    }
})
// Create Employee End

// Edit Employee Start
app.get('/employee/edit/:id', async (req, res) => {
    const employee = await db('employees').where('employee_id', req.params.id).first()
    // res.send(employee)
    res.render('employee/edit-employee', {employee, title: 'Edit Employee', layout: 'layouts/main', moment})
})

app.put('/employee', upload.single('employee_picture'), [
    check('employee_name', 'Please fill Employee Name').notEmpty(),
    check('employee_gender', 'Please fill Employee Gender').notEmpty(),
    check('employee_birthday', 'Please fill Employee Birthday').notEmpty(),
    check('employee_birthday', 'Birthday must be earlier than today').isBefore(),
    check('employee_phone', 'Please fill Employee Phone').notEmpty(),
    check('employee_phone', 'Phone must be a number').isNumeric()
], async (req, res) => {
    const errors = validationResult(req)
    let employeeFilename
    if (req.file) {
        employeeFilename = req.file.filename
        const employee = await db('employees').where('employee_id', req.body.employee_id).first()
        // res.send(employee)
        if(employee.employee_picture != null) {
            fs.unlink(__dirname + '/public/img/' + employee.employee_picture, (err) => {
                if (err) {
                    console.log(employee.employee_picture + ' does not exist')
                } else {
                    console.log(employee.employee_picture + ' has been deleted')
                }
            })
        }
    }
    if (!errors.isEmpty()) {
        const employee = req.body
        res.render('employee/edit-employee', {title: 'Edit Employee', layout: 'layouts/main', employee, errors: errors.array(), moment})
    } else {
        db('employees').where('employee_id', req.body.employee_id)
        .update({
            employee_name: req.body.employee_name,
            employee_gender: req.body.employee_gender,
            employee_birthday: req.body.employee_birthday,
            employee_picture: employeeFilename,
            employee_phone: req.body.employee_phone
        }).then(() => {
            res.redirect('/company/' + req.body.company_id)
        })
    }
})
// Edit Employee End

// Delete Employee Start
app.delete('/employee', async (req, res) => {
    const employee = await db('employees').where('employee_id', req.body.employee_id).first()
    // res.send(employee)
    if(employee.employee_picture != null) {
        fs.unlink(__dirname + '/public/img/' + employee.employee_picture, (err) => {
            if (err) {
                console.log(employee.employee_picture + ' does not exist')
            } else {
                console.log(employee.employee_picture + ' has been deleted')
            }
        })
    }
    db('employees').where('employee_id', employee.employee_id).delete().then(() => {
        res.redirect('/company/' + req.body.company_id)
    })
})
// Delete Employee End

// View Employee By ID Start
app.get('/company/employee/:id', async (req, res) => {
    const employee = await db('employees').where('employee_id', req.params.id).first()
    res.send(employee)
})
// View Employee By ID End

// Test API Flutter Start
app.get('/api/companies', async (req, res) => {
    const companies = await db.from('companies').select()
    res.status(200).json(companies)
})
// Test API Flutter End

app.get('/test', (req, res) => {
    res.send('Successfully Run')
})
app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})